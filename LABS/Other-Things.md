* Project Wiki
* [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)
* Container Registry: [Use](https://docs.gitlab.com/ee/user/project/container_registry.html) [Admin](https://docs.gitlab.com/ee/administration/container_registry.html)
* Operations
* Kubernetes Cluster integration
* Mattermost
* Snippets
