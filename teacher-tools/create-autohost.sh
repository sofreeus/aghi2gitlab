#!/bin/bash -eu
echo "Ensuring OpenStack login"
cd "$( dirname "$0")"
[[ "${OS_AUTH_URL+set}" == "set" ]] || source openstackrc

echo "Deleting the old autohost, if it exists"
openstack server delete autohost || echo whatever

echo "Creating autohost"
openstack server create --image "Fedora 30"  --flavor c1.large \
  --key-name dlwillson-yoga-2019-05 \
  autohost

echo "Assigning the Floating IP fuga.sofree.us($(dig fuga.sofree.us +short))"
openstack server add floating ip autohost $( dig fuga.sofree.us +short )

echo "Adding the machine to the public-web and public-ssh Security Groups"
openstack server add security group autohost public-ssh
openstack server add security group autohost public-web

# echo "Displaying the results"
# openstack server show autohost

echo "Waiting for the ssh server to come up"
while ! nc -w 1 -vz fuga.sofree.us 22 &>/dev/null
do
  echo -n .
done
echo 'up!'
  
echo "Refreshing the ssh server's keys"
ssh-keygen -R fuga.sofree.us || echo whatever
ssh-keygen -R $( dig fuga.sofree.us +short ) || echo whatever
ssh-keyscan fuga.sofree.us >> ~/.ssh/known_hosts
